package conf

import (
	"encoding/json"
	"log"
	"os"
)

type Config struct {
	Port string

	DDCInputSource string
	DDCDP          string
	DDCHDMI        string
	DDCPowerMode   string
	DDCOn          string
	DDCOff         string
	DDCSleep       string

	EvDevKeyboard string

	WWWStatsRefreshInterval int
}

var config Config

func CreateInstance(path string) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatalln(err)
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	config = Config{}
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatalln(err)
	}
}

func Port() string {
	return config.Port
}

func DDCInputSource() string {
	return config.DDCInputSource
}

func DDCDP() string {
	return config.DDCDP
}

func DDCHDMI() string {
	return config.DDCHDMI
}

func DDCPowerMode() string {
	return config.DDCPowerMode
}

func DDCOn() string {
	return config.DDCOn
}

func DDCOff() string {
	return config.DDCOff
}

func DDCSleep() string {
	return config.DDCSleep
}

func EvDevKeyboard() string {
	return config.EvDevKeyboard
}

func WWWStatsRefreshInterval() int {
	return config.WWWStatsRefreshInterval
}
