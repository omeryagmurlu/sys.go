import EventEmitter from "events";
import path from "path"
import { VMStats } from "./VMStats";
// import { JS_TO_HID_KEYCODES } from './keycodes'

const level1equal = (a, b) => {
    for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) return false;
    }
    return true
}

class Base extends EventEmitter {
    private _base: string;
    private _interval: number;
    constructor (base = "/api/") {
        super()
        this._base = base;
        this._interval = 0;
    }

    // irony, calling a polling system listen, but yeah
    // I prolly should have used a websocket on the backend.
    // well, I can always change it later, client is ready
    // for it
    listen(intervalTime = 1000) { // window.CONFIG.StatsRefreshInterval
        this.stopListening()
        const handler = async () => {
            try{
                await this.interval()
            } catch (e) {
                console.error(e);
            } finally {
                setTimeout(handler, intervalTime)
            }
        }
        setTimeout(handler, intervalTime)
    }

    stopListening() {
        clearTimeout(this._interval)
    }

    async interval() {}

    async _fetch(input: RequestInfo, init?: RequestInit): Promise<Response> {
        const a = await fetch(input, init);

        if (a.status >= 400) throw new Error(await a.text())

        return a;
    }

    postjson(p, data) {
        return this._fetch(path.join(this._base, p), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
    }

    async getjson(p) {
        const resp = await this._fetch(path.join(this._base, p));
        return resp.json();
    }

    async poke(p) {
        const resp = await this._fetch(path.join(this._base, p));
        return resp;
    }
}

export class Monitor extends Base {
    static self: Monitor = undefined
    private _input: string;
    private _power: string;
    _inputs: any;
    _powers: any;
    static get() {
        if (Monitor.self) return Monitor.self
        Monitor.self = new Monitor()
        return Monitor.self
    }
    
    constructor () {
        super("/api/monitor/")
        this._reloadMonitors()

        this._input = undefined
        this._power = undefined
    }

    // @Override
    async interval() {
        const o = [this._power, this._input]
        const n = [this._power, this._input] = [await this.getjson("power"), await this.getjson("input")] // bugfix: no Promise.all, ddcutil returns 1 if we are fast

        // // I was thinking of minimizing updates by diffing but it doesn't really matter, lol
        // if (!level1equal(o, n)) {
            this.emit("update", { power: this._power, input: this._input })
        // }
    }
    
    switchInput(x) {
        return this.poke(`input/switch/${x}`)
    }
    
    toggleInput() {
        return this.poke(`input/toggle`)
    }
    
    setPower(x) {
        return this.poke(`power/${x}`)
    }
    
    async _reloadMonitors() {
        this._inputs = await this.getjson("input/list")
        this._powers = await this.getjson("power/list")

        this.emit("config", this._powers, this._inputs)
    }
    
    getInput() {
        return this._input
    }
    
    getPower() {
        return this._power
    }
    
    getInputs() {
        return this._inputs
    }

    getPowers() {
        return this._powers
    }
}

export class Evdev extends Base {
    static self: Evdev = undefined
    static get() {
        if (Evdev.self) return Evdev.self
        Evdev.self = new Evdev()
        return Evdev.self
    }

    constructor() {
        super("/api/evdev/")
    }

    toggle() {
        return this.poke(`switch`)
    }
}

export class Libvirt extends Base {
    static self: Libvirt = undefined
    static lifecycleOps = ["start", "destroy", "shutdown", "restart", "reset"]
    private _stats: any;
    private _statscalc: any;
    private _domains: any;
    static get() {
        if (Libvirt.self) return Libvirt.self
        Libvirt.self = new Libvirt()
        return Libvirt.self
    }

    constructor() {
        super("/api/libvirt/")

        this._stats = undefined
        this._statscalc = new VMStats()
    }

    // @Override
    async interval() {
        const stats = await this.getjson("stats")
        this._statscalc.tick(stats)
        this.emit("stats", this._statscalc.get())
    }
    
    lifecycle(x, y) {
        if (!Libvirt.lifecycleOps.includes(y)) throw new TypeError()
        this.poke(`domain/${x}/${y}`)
    }

    async _reloadDomains() {
        this._domains = await this.getjson("list")

        this.emit("config", this._domains)
    }

    
    // // Imma use websockets for this
    // sendkeycodes(x, a /* array */, h) {
    //     const keycodes = a.map(x => JS_TO_HID_KEYCODES[x]);
    //     if (keycodes.filter(x => x).length !== a.length) throw new TypeError()
    //     this.postjson(`domain/${x}/sendkey`, {
    //         keycodes,
    //         holdtime: h // milli?
    //     })
    // }

    getDomains() {
        return this._domains
    }

    getStats() {
        return this._stats
    }
}