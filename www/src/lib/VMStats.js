// @ts-check

const Aggr = class {
    constructor(init) {
        this.value = init;
    }

    set(x) { // we could proxy this, but this is enough, no need to complicate things
        this.value = x;
    }

    valueOf() {
        return this.value;
    }
}

const getCurr = (dom, str) => dom.Params.find(x => x.Field == str)?.Value.I


export class VMStats {
    async tick(stats) {
        if (!this.info) this._populateInfo(stats)
        
        this.oldTime = this.T
        this.T = stats.Time
        
        for (const [i, dom] of stats.Stats.entries()) {
            this.cputotalr[i] = this.calcUsage(this.cputotal[i], getCurr(dom, 'cpu.time') ? Math.round(getCurr(dom, 'cpu.time') / 1000000) : -1, this.vcpuCount[i])
            for (let j = 0; j < this.vcpuCount[i]; j++) {
                this.vcpur[i][j] = this.calcUsage(this.vcpu[i][j], getCurr(dom, `vcpu.${j}.time`) ? Math.round(getCurr(dom, `vcpu.${j}.time`) / 1000000) : -1)
            }
            for (let j = 0; j < this.netCount[i]; j++) {
                this.netRXr[i][j] = this.calcUsage(this.netRX[i][j], getCurr(dom, `net.${j}.rx.bytes`) ? Math.round(getCurr(dom, `net.${j}.rx.bytes`) * 8) : -1)
                this.netTXr[i][j] = this.calcUsage(this.netTX[i][j], getCurr(dom, `net.${j}.tx.bytes`) ? Math.round(getCurr(dom, `net.${j}.tx.bytes`) * 8) : -1)
            }
        }
    }
    
    calcUsage(prev /* type: Aggr */, curr, timeMultiplier = 1 /* for cputotal and 600% normalization */) {
        if (curr === -1) { // broken/non-existant info, invalidate result
            return -1
        }
    
        if (this.oldTime === -1) { // first start
            prev.set(curr)
            return 0;
        }
    
        const usage = (curr - prev) / ((this.T - this.oldTime) * timeMultiplier)
        prev.set(curr)
        return usage;
    }

    async _populateInfo(stats) {
        this.info = stats
        
        this.oldTime // I don't want to pass this to calcDelta, so up it goes
        this.T = -1
        
        this.domainCount = this.info.Stats.length
        this.vcpuCount = Array(this.domainCount).fill().map((_, i) => getCurr(this.info.Stats[i], 'vcpu.current'));
        this.netCount = Array(this.domainCount).fill().map((_, i) => getCurr(this.info.Stats[i], 'net.count') || 0);

        this.cputotal = Array(this.domainCount).fill().map(() => new Aggr(-1)); // fuck, this last map and fill/same object took 20 mins of my life
        this.vcpu = Array(this.domainCount).fill().map((_, i) => Array(this.vcpuCount[i]).fill().map(() => new Aggr(-1)));
        this.netRX = Array(this.domainCount).fill().map((_, i) => Array(this.netCount[i]).fill().map(() => new Aggr(-1)));
        this.netTX = Array(this.domainCount).fill().map((_, i) => Array(this.netCount[i]).fill().map(() => new Aggr(-1)));

        this.cputotalr = Array(this.domainCount).fill(-1);
        this.vcpur = Array(this.domainCount).fill().map((_, i) => Array(this.vcpuCount[i]).fill(-1));
        this.netRXr = Array(this.domainCount).fill().map((_, i) => Array(this.netCount[i]).fill(-1));
        this.netTXr = Array(this.domainCount).fill().map((_, i) => Array(this.netCount[i]).fill(-1));
    }

    get() {
        return JSON.parse(JSON.stringify({
            total: this.cputotalr,
            cpu: this.vcpur,
            tx: this.netTXr,
            rx: this.netRXr
        }))
    }
}