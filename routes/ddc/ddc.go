package ddc

import (
	"net/http"

	"github.com/labstack/echo/v4"

	lib "sys-go/lib/ddc"
)

func DDCController(router *echo.Group) {
	router.GET("/input", func(c echo.Context) error {
		out, err := lib.GetInput()
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, out)
	})

	router.GET("/input/list", func(c echo.Context) error {
		return c.JSON(http.StatusOK, []string{
			"hdmi",
			"dp",
		})
	})

	type switchParams struct {
		Display string `param:"display" validate:"required,oneof=hdmi dp"`
	}
	router.GET("/input/switch/:display", func(c echo.Context) error {
		var params switchParams
		if err := c.Bind(&params); err != nil {
			return err
		}
		return lib.SwitchInput(params.Display)
	})

	router.GET("/input/toggle", func(c echo.Context) error {
		return lib.ToggleInput()
	})

	router.GET("/power", func(c echo.Context) error {
		out, err := lib.GetPower()
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, out)
	})

	router.GET("/power/list", func(c echo.Context) error {
		return c.JSON(http.StatusOK, []string{
			"on",
			"off",
			"sleep",
		})
	})

	type powerParams struct {
		Power string `param:"power" validate:"required,oneof=on sleep off"`
	}
	router.GET("/power/:power", func(c echo.Context) error {
		var params powerParams
		if err := c.Bind(&params); err != nil {
			return err
		}
		return lib.SetPower(params.Power)
	})
}
