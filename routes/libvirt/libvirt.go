package libvirt

import (
	"net/http"
	"time"

	lib "sys-go/lib/libvirt"

	"github.com/digitalocean/go-libvirt"
	"github.com/labstack/echo/v4"
)

func LibvirtController(router *echo.Group) {
	lib.Connect()

	type listParams struct {
		ActiveOnly bool `query:"activeOnly"`
	}
	router.GET("/list", func(c echo.Context) error {
		var params listParams
		if err := c.Bind(&params); err != nil {
			return err
		}
		d, err := lib.List(params.ActiveOnly)
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, d)
	})

	router.GET("/domain/:domain/start", func(c echo.Context) error {
		return lib.Start(c.Param("domain"))
	})

	router.GET("/domain/:domain/destroy", func(c echo.Context) error {
		return lib.Destroy(c.Param("domain"))
	})

	router.GET("/domain/:domain/reset", func(c echo.Context) error {
		return lib.Reset(c.Param("domain"))
	})

	router.GET("/domain/:domain/shutdown", func(c echo.Context) error {
		return lib.Shutdown(c.Param("domain"))
	})

	router.GET("/domain/:domain/reboot", func(c echo.Context) error {
		return lib.Reboot(c.Param("domain"))
	})

	// instead of sendkey use qmp keyup keydown

	// type sendkeyJson struct {
	// 	Keycodes []uint32 `json:"keycodes" validate:"required"`
	// 	Holdtime uint32   `json:"holdtime" validate:"required"`
	// }
	// router.POST("/domain/:domain/sendkey", func(c echo.Context) error {
	// 	var json sendkeyJson
	// 	if err := c.Bind(&json); err != nil {
	// 		return err
	// 	}

	// 	return lib.SendKey(c.Param("domain"), json.Holdtime, json.Keycodes)
	// })

	// on a cursory glance, one may want to implement this with a websocket
	// ie. push changes to the client, but when you think about it, letting
	// the client poll the data is the best course of action here
	router.GET("/stats", func(c echo.Context) error {
		type Stat struct {
			Stats []libvirt.DomainStatsRecord
			Time  int64
		}

		d, err := lib.Stats()
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, Stat{
			Stats: d,
			Time:  time.Now().UnixMilli(),
		})
	})
}
