package evdev

import (
	"net/http"
	lib "sys-go/lib/evdev"

	"github.com/labstack/echo/v4"
)

func EvDevController(router *echo.Group) {
	router.GET("/switch", func(c echo.Context) error {
		return lib.Switch()
	})

	router.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, lib.Exists())
	})
}
