package ddc

import (
	"errors"
	"log"
	"os/exec"
	"strings"
	"sys-go/utils/conf"
)

func SwitchInput(input string) error {
	var cmd *exec.Cmd
	if input == "hdmi" {
		cmd = exec.Command("ddcutil", "setvcp", conf.DDCInputSource(), conf.DDCHDMI())
	} else if input == "dp" {
		cmd = exec.Command("ddcutil", "setvcp", conf.DDCInputSource(), conf.DDCDP())
	} else {
		return errors.New("Invalid Input")
	}
	err := cmd.Run()
	if err != nil {
		return err
	}
	return nil
}

func GetInput() (string, error) {
	out, err := exec.Command("ddcutil", "getvcp", conf.DDCInputSource()).Output()
	if err != nil {
		log.Println(err)
		return "", err
	}
	if strings.Contains(string(out), "HDMI") {
		return "hdmi", nil
	} else if strings.Contains(string(out), "DisplayPort") {
		return "dp", nil
	}

	return "", errors.New("unidentified input")
}

func ToggleInput() error {
	input, err := GetInput()
	if err != nil {
		return err
	}
	var s string
	if input == "hdmi" {
		s = "dp"
	} else {
		s = "hdmi"
	}
	return SwitchInput(s)
}

func GetPower() (string, error) {
	out, err := exec.Command("ddcutil", "getvcp", conf.DDCPowerMode()).Output()
	if err != nil {
		log.Println(err)
		return "", err
	}
	if strings.Contains(string(out), "DPM: On") {
		return "on", nil
	} else if strings.Contains(string(out), "DPM: Off") {
		return "sleep", nil
	} else if strings.Contains(string(out), "turn off display") {
		return "off", nil
	}

	return "", errors.New("Unidentified Input")
}

func SetPower(power string) error {
	var cap string
	if power == "on" {
		cap = conf.DDCOn()
	} else if power == "sleep" {
		cap = conf.DDCSleep()
	} else if power == "off" {
		cap = conf.DDCOff()
	} else {
		return errors.New("invalid input")
	}

	exec.Command("ddcutil", "setvcp", conf.DDCPowerMode(), cap).Run()
	// if err != nil {
	// ddcutil returns random errors between power states, but works nonetheless, just ignore
	// return err
	// }
	return nil
}
