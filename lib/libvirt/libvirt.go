package libvirt

import (
	"errors"
	"log"

	"github.com/digitalocean/go-libvirt"
	"github.com/digitalocean/go-libvirt/socket/dialers"
)

var lib *libvirt.Libvirt

func Connect() {
	lib = libvirt.NewWithDialer(dialers.NewLocal())
	if err := lib.Connect(); err != nil {
		log.Fatalf("failed to connect to libvirt: %v", err)
	}
}

func List(activeOnly bool) ([]libvirt.Domain, error) {
	flag := int32(libvirt.ConnectGetAllDomainsStatsInactive | libvirt.ConnectGetAllDomainsStatsActive)
	if activeOnly {
		flag = int32(libvirt.ConnectGetAllDomainsStatsActive)
	}
	domains, _, err := lib.ConnectListAllDomains(1, libvirt.ConnectListAllDomainsFlags(flag))
	return domains, err
}

func findByName(name string) (*libvirt.Domain, error) {
	d, err := List(false)
	if err != nil {
		return nil, err
	}
	for i := range d {
		if d[i].Name == name {
			return &d[i], nil
		}
	}

	return nil, errors.New("no such domain")
}

func Start(name string) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	return lib.DomainCreate(*d)
}

func Destroy(name string) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	return lib.DomainDestroy(*d)
}

func Reset(name string) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	return lib.DomainReset(*d, 0)
}

func Shutdown(name string) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	return lib.DomainShutdown(*d)
}

func Reboot(name string) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	return lib.DomainReboot(*d, 0)
}

func SendKey(name string, holdtime uint32, keycodes []uint32) error {
	d, err := findByName(name)
	if err != nil {
		return err
	}

	// use https://github.com/mtlynch/key-mime-pi/blob/904e56b6bf1f76da1abb85f654637da0e3c35fa3/app/js_to_hid.py#L32
	// in the frontend to map JS -> USB HID
	return lib.DomainSendKey(*d, uint32(libvirt.KeycodeSetUsb), holdtime, keycodes, 0)
}

func Stats() ([]libvirt.DomainStatsRecord, error) {
	r, err := lib.ConnectGetAllDomainStats(nil,
		uint32(libvirt.DomainStatsState)|uint32(libvirt.DomainStatsInterface)|uint32(libvirt.DomainStatsCPUTotal)|uint32(libvirt.DomainStatsVCPU),
		0)
	if err != nil {
		return nil, err
	}

	return r, nil
}
