package evdev

import (
	"encoding/binary"
	"errors"
	"os"
	"sys-go/utils/conf"
	"syscall"
	"time"

	ev "github.com/gvalkov/golang-evdev"
)

func Exists() bool {
	if _, err := os.Stat(conf.EvDevKeyboard()); err == nil {
		return true
	}
	return false
}

func input_event(typ uint16, code uint16, value int32) ev.InputEvent {
	return ev.InputEvent{
		Time: syscall.Timeval{
			Sec:  time.Now().Unix(),
			Usec: time.Now().UnixNano() / 1000 % 1000,
		},
		Type:  typ,
		Code:  code,
		Value: value,
	}
}

func write(f *os.File, typ uint16, code uint16, value int32) {
	binary.Write(f, binary.LittleEndian, input_event(typ, code, value))
}

func Switch() error {
	f, err := os.OpenFile(conf.EvDevKeyboard(), os.O_WRONLY, 0)
	if err != nil {
		return errors.New("error opening input")
	}
	write(f, ev.EV_KEY, ev.KEY_LEFTCTRL, 1)
	write(f, ev.EV_KEY, ev.KEY_RIGHTCTRL, 1)
	write(f, ev.EV_KEY, ev.KEY_LEFTCTRL, 0)
	write(f, ev.EV_KEY, ev.KEY_RIGHTCTRL, 0)
	write(f, ev.EV_SYN, 0, 0)
	f.Close()
	return nil
}
