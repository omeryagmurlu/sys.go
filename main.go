// Sincerely, fuck gin
// That shit locked me in with it's shit httprouter and took a day of my life

//go:generate bash -c "cd www; npm run build"

package main

import (
	"embed"
	"encoding/json"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"strings"
	"sys-go/routes/ddc"
	"sys-go/routes/evdev"
	"sys-go/routes/libvirt"
	"sys-go/utils"
	"sys-go/utils/conf"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

//go:embed www/public
var embfs embed.FS

func main() {
	confPath := flag.String("config", "./conf.json", "configuration file, by default ./conf.json")
	flag.Parse()
	conf.CreateInstance(string(*confPath))

	public, err := fs.Sub(embfs, "www/public")
	if err != nil {
		log.Fatalln(err)
	}

	e := echo.New()
	e.Validator = &utils.CustomValidator{}

	ddc.DDCController(e.Group("/api/monitor"))
	evdev.EvDevController(e.Group("/api/evdev"))
	libvirt.LibvirtController(e.Group("/api/libvirt"))

	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:       ".",
		Filesystem: http.FS(public),
		Skipper: func(c echo.Context) bool {
			return strings.HasPrefix(c.Path(), "/api") // just a little perf optimization
		},
	}))

	w, _ := json.Marshal(struct {
		StatsRefreshInterval int
	}{
		StatsRefreshInterval: conf.WWWStatsRefreshInterval(),
	})
	webConf := fmt.Sprintf("window.CONFIG = %s", w)
	e.GET("/variables.js", func(c echo.Context) error {
		c.Response().Header().Set(echo.HeaderContentType, echo.MIMEApplicationJavaScript)
		return c.String(http.StatusOK, webConf)
	})

	e.Logger.Fatal(e.Start(conf.Port()))
}
